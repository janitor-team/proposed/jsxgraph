jsxgraph (1.4.5+dfsg1-1) unstable; urgency=medium

  * New upstream version 1.4.5+dfsg1

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 25 Jul 2022 12:31:51 +0200

jsxgraph (1.4.4+dfsg1-1) unstable; urgency=medium

  * New upstream version 1.4.4+dfsg1

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 31 May 2022 19:09:57 +0200

jsxgraph (1.4.3+dfsg1-1) unstable; urgency=medium

  * new upstream version 1.4.3

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 19 Apr 2022 12:43:03 +0200

jsxgraph (1.4.2+dfsg1-2) unstable; urgency=medium

  * added JSXCompressor/jsxgraph.css to the list of Files-Excluded in
    debian/copyright

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 14 Feb 2022 17:56:51 +0100

jsxgraph (1.4.2+dfsg1-1) unstable; urgency=medium

  * new upstream version
  * bumped Standards-Version: 4.6.0

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 14 Feb 2022 17:24:13 +0100

jsxgraph (1.4.1+dfsg1-1) unstable; urgency=medium

  * new upstream version
  * refreshed debian patches

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 28 Jan 2022 19:50:44 +0100

jsxgraph (1.4.0+dfsg1-1) experimental; urgency=medium

  * new upstream version
  * modified the build-dependency uglifyjs.terser -> terser.
    Closes: #1001034
  * rewritten debian/copyright to use "copyright-format/1.0", and defined
    a list of "Files-Excluded"
  * modified the watch file to track https://github.com/jsxgraph/jsxgraph/tags
    and to repack the archive with the suffix "dfsg1"
  * removed the file debian/get-latest-source
  * bumped debhelper  to debhelper-compat (= 13), Standards-Version: 4.5.1,
    priority becomes "optional"
  * updated debian patches
  * updated source/lintian-overrides

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 03 Dec 2021 12:01:29 +0100

jsxgraph (1.3.5+dfsg1-8) unstable; urgency=medium

  * replaced uglifyjs by uglifyjs.terser in Makefile. Closes: #980470

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 19 Jan 2021 16:41:44 +0100

jsxgraph (1.3.5+dfsg1-7) unstable; urgency=medium

  * replaced a build-dependency: node-requirejs -> uglifyjs.terser
    Closes: #979903

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 15 Jan 2021 15:46:44 +0100

jsxgraph (1.3.5+dfsg1-6) unstable; urgency=medium

  * Now, python is installed before compiling jsxgraph's scripts
    some scripts were fixed so python3 can compile them properly.
    Closes: #954776

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 31 Mar 2020 17:26:54 +0200

jsxgraph (1.3.5+dfsg1-5) unstable; urgency=medium

  * Changed the dependency on python, made it a pre-dependency.
    Closes: #954776

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 31 Mar 2020 16:11:21 +0200

jsxgraph (1.3.5+dfsg1-4) unstable; urgency=medium

  * changed the symlink to access the new location of almond.js
    Closes: #952307
  * improved dependencies of jsxgraph to take python3 in account

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 02 Mar 2020 19:43:46 +0100

jsxgraph (1.3.5+dfsg1-3) unstable; urgency=medium

  * make changes to use python3 only. Closes: #943079 

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 01 Dec 2019 19:57:53 +0100

jsxgraph (1.3.5+dfsg1-2) unstable; urgency=medium

  * adapted the Makefile to take in account in the dependency node-almond.
    Closes: #922254

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 14 Feb 2019 11:12:16 +0100

jsxgraph (1.3.5+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * added VCS stuff to debian/control
  * changed the symlink to node-almond; Closes: #893607

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 01 Apr 2018 23:58:39 +0200

jsxgraph (0.99.6+dfsg1-1) unstable; urgency=medium

  * New upstream release

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 23 Aug 2017 10:46:26 +0200

jsxgraph (0.99.5+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * changed the pattern of new packages got by uscan
  * upgraded Standards-Version to 3.9.8
  * created a new output package, jsxcompressor. Closes: #846378
  * reworked some parts of the packaging to make lintian happy

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 03 Dec 2016 17:08:57 +0100

jsxgraph (0.99.3~dfsg1-1) unstable; urgency=medium

  * upgraded to the newest upstream version
  * removed a few sourceless files; Closes: #788728, and documented
    a few javascript sources in jsxgraph-lintian-overrides
  * realized that a former bug was already fixed. (Resolve FTBFS
    because of missing b-d-i). Closes: #734501
  * modified the installation to comply with Apache2.4; Closes: #669752
  * upgraded to dh_python2, thanks to Mattia Rizzolo's patch.
    Closes: #808165
  * upgraded Standards-Version to 3.9.6
  * imported the bootstrap script r.js as a quilt patch
  * changed the links to prototype.js in examples
  * changed the links to jquery.js in examples
  * changed the links to mathjax.js in examples

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 17 Dec 2015 10:17:07 +0100

jsxgraph (0.98~dfsg1-3) unstable; urgency=low

  * added build-dependencies node-uglify and zip, thanks to
    Daniel T Chen <seven.steps@gmail.com>
  * upgraded Standards-Version to 3.9.5

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 13 Jan 2014 18:54:06 +0100

jsxgraph (0.98~dfsg1-2) unstable; urgency=low

  * modified the Makefile in order to:
    - remove uglified file jsxcompressor.js upon clean
    - build jsxcompressor.js with "make all"
    Closes: #728659
  * added dependencies on python-support and python

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 09 Dec 2013 16:16:06 +0100

jsxgraph (0.98~dfsg1-1) unstable; urgency=low

  * upgraded to the new upstream release
  * updated the file get-latest-source
  * updated debian/watch
  * updated Standards-Version to 3.9.4
  * added build-dependencies:
    + node-almond
    + node-requirejs

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 01 Nov 2013 13:46:53 +0100

jsxgraph (0.96~dfsg1-2) unstable; urgency=low

  * upload to unstable without other modification

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 30 May 2013 23:20:02 +0200

jsxgraph (0.96~dfsg1-1) experimental; urgency=low

  * upgraded to the latest upstream version

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 13 Jan 2013 11:42:07 +0100

jsxgraph (0.94~dfsg1-2) experimental; urgency=low

  * modified my DEBEMAIL
  * updated the script debian/get-latest-source to get sources from
    https://github.com/jsxgraph/jsxgraph/tree

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 13 Jan 2013 10:34:38 +0100

jsxgraph (0.94~dfsg1-1) experimental; urgency=low

  * upgraded to the latest tagged revision

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 15 Nov 2012 18:52:55 +0100

jsxgraph (0.83+svn1872~dfsg1-1) experimental; urgency=low

  * Added a script debian/get-latest-source to build a custom orig tarball
    based on the latest tagged source tree.
  * removed the target get-orig-source in debian/rules since it is related
    to one past release in the SVN repository
  * updated Standards-Version to 3.9.3

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 15 Nov 2012 18:02:47 +0100

jsxgraph (0.83+svn1872~dfsg1-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add get-orig-source target to debian/rules to recreate the custom
    orig tarball.
  * Changes of the orig tarball:
    - Do not include the 'tools' directory in the orig tarball.
      It is not used and contains the non-free jsmin (Closes: #689763)
      and compiled versions of yuicompressor without source (Closes: #691493).
    - Remove compressed copies of jsxgraphcore.js in old versions without
      source (Closes: #691493).

 -- Tobias Hansen <thansen@debian.org>  Fri, 09 Nov 2012 22:25:11 +0100

jsxgraph (0.83+svn1872~dfsg-3) unstable; urgency=low

  * changed symlinks to protoype.js (they were broken)
  * removed debian/watch since the source is retreived from a SVN repository

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 20 Jul 2011 11:44:58 +0200

jsxgraph (0.83+svn1872~dfsg-2) unstable; urgency=low

  * Closes: #612233 (the already fixed ITP)
  * changed to a versioned dependency on jsdoc-toolkit
  * bumped Standards-Version to 3.9.2


 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 16 Jul 2011 17:08:12 +0200

jsxgraph (0.83+svn1872~dfsg-1) unstable; urgency=low

  * stripped pdf files, zip files out of the source package.
  * modified debian/copyright to take in account some files with MIT 
    licenses and mixed licenses.
  * removed files jquery.min.js and prototype.js from the source package

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 09 Apr 2011 16:28:54 +0200

jsxgraph (0.83+svn1872-1) unstable; urgency=low

  * Upgraded to a newer upstream release: enables many examples 
    existing in the SVN repository.

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 07 Feb 2011 15:32:07 +0100

jsxgraph (0.82-1) unstable; urgency=low

  * Upgraded to a newer upstream release
  * First publication. Closes: #612233

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 07 Feb 2011 02:06:30 +0100

jsxgraph (0.72-1) unstable; urgency=low

  * Upgraded to a newer upstream release

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 21 Mar 2010 20:05:25 +0100

jsxgraph (0.50-2) unstable; urgency=low

  * modified the files in distrib/ to contain the latest sources
    grabbed from http://jsxgraph.uni-bayreuth.de/distrib this day.
  * added the generation of a documentation in HTML format with the tool jsdoc

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 23 Sep 2008 15:53:56 +0200

jsxgraph (0.50-1) unstable; urgency=low

  * Initial release (Closes: #0) 

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 23 Sep 2008 08:08:51 +0200

