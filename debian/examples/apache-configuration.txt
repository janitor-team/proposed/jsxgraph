Here is a possible way to configure apache so it can serve urls like
/jsxgraph/distrib/jsxgraph.css, /jsxgraph/distrib/prototype.js,
/jsxgraph/distrib/jsxgraphcore.js

Create the following symbolic link:
/etc/apache*/conf.d/jsxgraph.conf -> /etc/jsxgraph/apache.conf

Reload Apache's configuration.
